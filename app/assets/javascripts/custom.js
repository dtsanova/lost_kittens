var LostKittens = {

    currentFacing: 'north',
    currentX: 0,
    currentY: 0,
    leftTurns: {'north': 'west', 'west': 'south', 'south': 'east', 'east': 'north'},
    rightTurns: {'north': 'east', 'east': 'south', 'south': 'west', 'west': 'north'},
    speed: 500,

//  Makes an AJAX call to send the email to the backend,
//  handles two types of response:
//  success - gets data, shows success alert msg to the modal, creates the coordination system, creates animations
//  fail - depending on the type of error it receives, shows different error msg in the modal.
    getDirections: function(form){
        $.ajax({
            url: form.action,
            data: {
                email: $(form).find("#email").val(),
                offline: $(form).find("#offline").val()
            },
            dataType: 'json'
        })
        .done(function(response) {
            LostKittens.directions = response.directions;
            LostKittens.message = response.message;
            LostKittens.x = response.x;
            LostKittens.y = response.y;
            $('#emailModalContent').prepend($('<div id="emailModalContentAlert" class="alert alert-success" role="alert" />').text("Your have been successfully authenticated!"));
            $('#emailModalContentAlert').hide().fadeIn();
            setTimeout(function () {
                $('#emailModal').modal('hide');
                LostKittens.createMapTiles();
                LostKittens.showAnimation(LostKittens.directions);
            }, 1500);

        })
        .fail(function(response) {
                var msgType = "";
                switch (response.status){
                    case 400: //bad_request
                        msgType = "danger";
                    break;
                    case 404: //not_found
                        msgType = "warning";
                    break;
                    case 417: //expectation_failed
                        msgType = "danger";
                    break;
                    default:

                }
            $('#emailModalContent').prepend($('<div id="emailModalContentAlert" class="alert alert-' + msgType + '" role="alert" />').text(response.responseText));
            $('#emailModalContentAlert').hide().fadeIn();
        });
    },
//  Creates the coordination system dynamically
    createMapTiles: function () {
        var width = 9;
        var height =  9;
        var container = $('#map-container');
        var containerWidth = (width + 1) * 5 + 'em'; // sets the size of the content dynamically
        container.css({'width': containerWidth, 'min-width': containerWidth});
        var list = $("<ul/>").attr('id', 'mapTilesList');
        // create the rows and columns dynamically
        for (rowIndex = height - 1; rowIndex >= -1; rowIndex--) {
            var row = $("<li/>");
            for (colIndex = -1; colIndex < width; colIndex++) {
                var content = '&nbsp;';
                if (colIndex === -1) { //First column, fill in row number
                    content = (rowIndex === -1) ? '&nbsp;' : rowIndex;
                } else if (rowIndex === -1 && colIndex >= 0) { //Bottom row
                    content = (colIndex === -1) ? '&nbsp;' : colIndex;
                }
                row.append($("<div/>").attr('id', 'tileX' + colIndex + 'Y' + rowIndex).html(content));
            }
            list.append(row);
        }
        return container.append(list);
    },

//  Shows the moves on the tiles depending on the x and y, animated
    showAnimation: function (directions) {

        this.showStartingPosition(); // calls this method to show the initial step

        $.each(directions, function (index, direction) {
            switch (direction) {
                case 'left':
                    setTimeout(function () {
                        LostKittens.turnLeft();
                    }, index * LostKittens.speed);
                    break;
                case 'right':
                    setTimeout(function () {
                        LostKittens.turnRight();
                    }, index * LostKittens.speed);
                    break;
                case 'forward':
                    setTimeout(function () {
                        LostKittens.goForward();
                    }, index * LostKittens.speed);
                    break;
            }
        });
        // after we have the final x and y, we mark the selected tile and send the search party
        setTimeout(function () {
            LostKittens.markLocationTile();
        }, directions.length * this.speed);

        setTimeout(function () {
            LostKittens.sendSearchParty();
        }, (directions.length * this.speed) + 1000);
    },
    showStartingPosition: function () {
        this.showNewSteps();
    },
//  Gets the current facing position, and calls method to handle the transition
    turnLeft: function () {
        this.currentFacing = this.leftTurns[this.currentFacing];
        this.rotateCurrentTileSteps();
    },
//  Gets the current facing position, and calls method to handle the transition
    turnRight: function () {
        this.currentFacing = this.rightTurns[this.currentFacing];
        this.rotateCurrentTileSteps();
    },
//  Calculates the steps on the coordination system
    goForward: function () {
        switch (this.currentFacing) {
            case 'north':
                this.currentY += 1;
                break;
            case 'west':
                this.currentX -= 1;
                break;
            case 'south':
                this.currentY -= 1;
                break;
            case 'east':
                this.currentX += 1;
                break;
        }
        this.showNewSteps();
    },
//  Gets the current step and sets the image
    showNewSteps: function () {
        $('#tileX' + this.currentX + 'Y' + this.currentY).html("<img src='/images/detective-steps-small.png' class='" + this.currentFacing + "'/>");
        var currentStep = $('#tileX' + this.currentX + 'Y' + this.currentY + ' img');
        currentStep.fadeIn();
    },
//  Rotates the img in the direction it is facing, with css transform
    rotateCurrentTileSteps: function () {
        var currentStep = $('#tileX' + this.currentX + 'Y' + this.currentY + ' img');
        currentStep.attr('class', this.currentFacing);
    },
//  Marks the last step using css
    markLocationTile: function(){
        var lastTile = $('#tileX' + this.currentX + 'Y' + this.currentY);
        lastTile.css({'background-color': 'rgba(182, 1, 1, 0.6)'}).fadeOut('fast').fadeIn('fast').fadeOut('fast').fadeIn('fast');
    },
//  Gets the last tile, its position, and animates img(the middle of it)
//  to go from the left upper corner to the middle of the tile
    sendSearchParty: function () {
        var lastStep = $('#tileX' + this.currentX + 'Y' + this.currentY + ' img');
        var position = lastStep.position();
        var searchParty = $("#search-party");
        var searchPartyPoliceCarImg = $("#search-party img");
        searchParty.show().animate({
            left: (position.left + (lastStep.width() / 2)) - (searchPartyPoliceCarImg.width() / 2),
            top: (position.top + (lastStep.height() / 2)) - (searchPartyPoliceCarImg.height() / 2)
        }, 3000, function(){
//          In the callback of animate(), we show a modal with dynamic content
            setTimeout(function () {
                $('#successModalContent')
                    .append($('<h3 class="center" />').text("You have successfully saved the fluffy kittens!"))
                    .append($('<h4 class="center" />').text("The Forensics found them at X: " + LostKittens.currentX + ", Y: " + LostKittens.currentY))
                    .append($('<h5 class="center" />').text("The Forensics API returns the following message for you:"))
                    .append($('<div class="alert alert-success center" role="alert" />').text(LostKittens.message))
                $('#successModal').modal('show');
            }, LostKittens.speed * 2);
        });
    },
//  Preloads the images while the fist modal is shown so that the appearance of the imgs is smooth
    preloadImages: function(){
        var imagesToPreload = [
            '/images/used-paper.jpg',
            '/images/detective-steps-small.png',
            '/images/police-car-small.png',
            '/images/happy-kitten-small.png'
        ];
        $(imagesToPreload).each(function(){
            $('<img/>')[0].src = this;
        });
    }
};

// On loading the page, a modal is shown to get the email address of the user
$(function () {
    $('#emailModal').modal('show');
    $('#emailModal').on('shown.bs.modal', function () {
        $('#emailInput').focus()
    });
    LostKittens.preloadImages();
    $('#emailForm').submit(function(event){
        event.preventDefault();
        LostKittens.getDirections(this);
    });
});

