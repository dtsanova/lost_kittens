class FlightMap
  include ActiveModel::Model
  attr_accessor :directions, :x, :y, :facing, :message

  # Provide initial values
  def initialize(attributes={})
    super
    @x ||= 0
    @y ||= 0
    @facing ||= 'north'
  end

  # Calculates the coordinates when we have a directions of "forward"
  def goForward
    case self.facing
      when 'north'
        self.y += 1
      when 'west'
        self.x -= 1
      when 'south'
        self.y -= 1
      when 'east'
        self.x += 1
    end
  end

  # Calculates the coordinates when we have a directions of "left",
  # whichever direction is faced, we have the exact direction in which it is to turn
  def turnLeft
    leftTurns = {'north' => 'west', 'west' => 'south', 'south' => 'east', 'east' => 'north'}
    self.facing = leftTurns[self.facing]
  end

  # Calculates the coordinates when we have a directions of "right"
  # whichever direction is faced, we have the exact direction in which it is to turn
  def turnRight
    rightTurns = {'north' => 'east', 'east' =>'south', 'south' => 'west', 'west' =>'north'}
    self.facing = rightTurns[self.facing]
  end


end