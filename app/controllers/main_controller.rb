class MainController < ApplicationController
  include HTTParty

  # Landing page
  # Params:
  # +offline+:: boolean string used to enable offline mood
  def index
    @is_offline = !params[:offline].blank? && (params[:offline] == 'true')
  end

  # Calls Forensics direction API, calculates the coordinate result,
  # calls Forensics location API passing email and coordinates result.
  # Returns the following responses:
  # 200 - when the found coordinates are correct and the location is found
  # 208 - when the location has already been found, we send a request again
  # 404 - when the coordinates are incorrect
  # 400 - when the call to do system cannot be made for any reason
  # 417 - when the email is invallid
  # Params:
  # +email+:: string used to call the Forensics API
  # +is_offline+:: (optional) boolean string that enables the offline working mode
  def getDirections
    @@email = params[:email]
    @@is_offline = !params[:offline].blank? && (params[:offline] == 'true')

    if is_a_valid_email?(@@email)

      flightMap = FlightMap.new()
      askForensicsForDirections(flightMap)
      calculateKittensLocation(flightMap)
      sendSearchParty(flightMap)

      if flightMap.message.include? 'Congratulations'
        render :status => :ok, :json => flightMap
      elsif flightMap.message.include? 'The search party has already recovered the kittens'
        render :status => :already_reported, :json => flightMap
      elsif flightMap.message.include? 'Unfortunately'
        render :status => :not_found, :text => flightMap.message
      else
        render :status => :bad_request, :text => 'Forensics are not available at the moment, please try again later'
      end

    else
      render :status => :expectation_failed, :text => 'Email is invalid'
    end

  end


  private

  # To get the directions array, makes rest call to either get data from Forensics API,
  # or reads from a json file.
  def askForensicsForDirections(flightMap)

    if @@is_offline
      directionsResponse = File.read('public/mockData/directions.json')
    else
      response = HTTParty.get(URI.encode("http://which-technical-exercise.herokuapp.com/api/#{@@email}/directions"))
      directionsResponse = response.body
    end

    flightMap.directions = JSON.parse(directionsResponse)['directions']
  end

  # Calculates the coordinates based on the received directions
  def calculateKittensLocation(flightMap)
    flightMap.directions.each do | direction |
      case direction
        when 'forward'
          flightMap.goForward
        when 'left'
          flightMap.turnLeft
        when 'right'
          flightMap.turnRight
      end
    end
  end

  # To check if the coordinates are correct, makes rest call to either get data from Forensics API,
  # or reads from a json file.
  def sendSearchParty(flightMap)
    if @@is_offline
      # locationResponse = File.read('public/mockData/location_success.json')
      locationResponse = File.read('public/mockData/location_already_saved.json')
      # locationResponse = File.read('public/mockData/location_error.json')
    else
      response = HTTParty.get(URI.encode("http://which-technical-exercise.herokuapp.com/api/#{@@email}/location/#{flightMap.x}/#{flightMap.y}"))
      locationResponse = response.body
    end
    flightMap.message = JSON.parse(locationResponse)['message']
  end

  # Validates an email
  # Params:
  # +email+:: string to be validated against VALID_EMAIL_REGEX regular expression, located in config/initializers/constants.rb
  def is_a_valid_email?(email)
    (email =~ VALID_EMAIL_REGEX)
  end

end