# README #

Lost Kittens

### What is this repository for? ###

Many kittens were stolen by an evil woman. We have to find her location, and save the fluffy kittens. 
We have initial starting point and we receive directions, which would hopefully lead us to her location.
We calculate the directions we take and get an exact coordinates of the woman's location and we save the kittens. 


### How do I get set up? ###

Here is the link of the program deployed on a cloud platform OpenShift:

http://kittens.sunnyheart.net/

Or you could clone the repository from Bitbucket.



Further functionalities that could be made:

* to calculate dynamically the size of the grid, by getting the max and y and x coordinates.
